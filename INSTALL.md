# Installation Guide

## Data Collection

Un [article de l'ANSES](https://www.anses.fr/fr/content/%C3%A9tude-de-l%E2%80%99alimentation-totale-eat-2-l%E2%80%99anses-met-%C3%A0-disposition-les-donn%C3%A9es-de-son-analyse) présente l'étude qui a été menée sur l'exposition des populations à des substances chimiques présentes dans les aliments. Y figure un lien vers [un jeu de données](https://www.data.gouv.fr/fr/datasets/donnees-regionales-eat2-etude-de-l-alimentation-totale/) issu de la deuxième édition de l'étude de l'alimentation totale (EAT2) stocké dans `raw data`. 

Pour comprendre la méthode d’évaluation de l’exposition des populations à des substances chimiques présentes dans les aliments, il faut se référer aux tomes [1](https://www.anses.fr/fr/system/files/PASER2006sa0361Ra1.pdf) et [2](https://www.anses.fr/fr/system/files/PASER2006sa0361Ra2.pdf) des Avis et rapport relatif à l'Etude de l'alimentation totale française 2 (EAT 2).


## Dependencies

- Créer un environnement virtuel avec virtualenv et installer les modules du `requirements.txt`:

  ```bash
  cd Streamlit/
  python -m virtualenv .venv/
  source .venv/bin/activate
  pip install -r requirements.txt
  ```
  
- Pour sortir de l'environnement virtuel :

  ```bash
  deactivate
  ```
