# Challenge GD4H - Caliviz

<a href="https://gd4h.ecologie.gouv.fr/" target="_blank" rel="noreferrer">Green Data for Health</a> (GD4H) is an initiative led by the Innovation Lab (Ecolab) of the French ministry of ecology.

A challenge has been organised in 2023 to develop tools, rooted in the health-environment data community, aiming at addressing shared issues.

Link : 
<a href="https://gd4h.ecologie.gouv.fr/defis/738" target="_blank" rel="noreferrer">Website</a> 

## Caliviz

According to the results of the latest edition of the Total Diet Study (TDS), health risk cannot be excluded in certain population groups for 12 substances present in our current diet. Today, food monitoring is carried out randomly.

Optimizing surveillance by identifying and targeting problematic food/substance combinations is therefore in the public interest, with the ultimate aim of protecting the consumer.

<a href="https://gd4h.ecologie.gouv.fr/defis/738" target="_blank" rel="noreferrer">More about the challenge</a>

## **Documentation**

### Methodology - Data processing
The first stage of the Caliviz project involved processing data below detection or quantification limits, known as censored data, to take account of analytical limits and the specific characteristics of different substance families. Depending on the substances and food groups for which the analytical limits are known or unknown, the censored data were entered into the files in different formats. As a result, several specific pre-processing operations were carried out for the different substance families, in order to harmonize all the data that would then be integrated into the visualization tool.

#### Type 1 formatting: Inorganic and mineral contaminants - Acrylamide

In this case, censored data are only in the form "ND/NQ" and analytical limits are known. The contamination of each food by each substance is estimated according to the censoring hypothesis as follows:

- Medium hypothesis (MB): ND = LOD/2 and NQ = LOQ/2
- Low hypothesis (LB): ND = 0 and NQ = LOD
- High hypothesis (UB): ND = LOD and NQ = LOQ

#### Type 2 formatting: HAP - Dioxins, PC8 - Perfluorinated - Brominated compounds
In this case, the censored data are entered as "<value" and the detection and/or quantification limits are unknown. The contamination of each food by each substance is estimated according to the censoring hypothesis as follows:

- Medium hypothesis (MB): <value = value/2
- Low hypothesis (LB): <value = 0
- High hypothesis (UB): <value = value

#### Type 3 formatting: Additives - Pesticides

In this case, censored data are in the form ND(value)/NQ(value) and analytical limits are not provided. The contamination of each food by each substance is estimated according to the censoring hypothesis as follows:

- Medium hypothesis (MB): ND(value) = value/2 and NQ(value) = value/2
- Low hypothesis (LB): ND(value) = 0 and NQ(value) = 0
- High hypothesis (UB): ND(value) = value and NQ(value) = value

### **Installation**

[Installation Guide](/INSTALL.md)

### **Usage**

Use directly on the platform: https://caliviz.streamlit.app/

### **Contributing**

If you wish to conribute to this project, please follow the [recommendations](/CONTRIBUTING.md).

### **Licence**

The code is published under [MIT Licence](/LICENSE).

The data referenced in this README and in the installation guide is published under <a href="https://www.etalab.gouv.fr/wp-content/uploads/2018/11/open-licence.pdf">Open Licence 2.0</a>.
